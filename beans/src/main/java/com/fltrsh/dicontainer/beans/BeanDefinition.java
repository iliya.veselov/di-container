package com.fltrsh.dicontainer.beans;

import java.util.List;
import java.util.Objects;

public class BeanDefinition {
    private Class<?> beanClass;
    private List<String> dependencies;
    private List<BeanDefinitionConstructorArgument> constructorArguments;

    public BeanDefinition(Class<?> beanClass, List<String> dependencies, List<BeanDefinitionConstructorArgument> constructorArguments) {
        this.beanClass = beanClass;
        this.dependencies = dependencies;
        this.constructorArguments = constructorArguments;
    }

    public Class<?> getBeanClass() {
        return beanClass;
    }

    public List<String> getDependencies() {
        return dependencies;
    }

    public List<BeanDefinitionConstructorArgument> getConstructorArguments() {
        return constructorArguments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeanDefinition that = (BeanDefinition) o;
        return Objects.equals(beanClass, that.beanClass) &&
                Objects.equals(dependencies, that.dependencies) &&
                Objects.equals(constructorArguments, that.constructorArguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(beanClass, dependencies, constructorArguments);
    }
}
