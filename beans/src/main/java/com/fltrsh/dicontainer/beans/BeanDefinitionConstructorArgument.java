package com.fltrsh.dicontainer.beans;

import java.util.Objects;

public class BeanDefinitionConstructorArgument {
    private Type type;
    private String name;
    private String value;

    public BeanDefinitionConstructorArgument(Type type, String name, String value) {
        this.type = type;
        this.name = name;
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public enum Type {
        BEAN,
        PRIMITIVE
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeanDefinitionConstructorArgument that = (BeanDefinitionConstructorArgument) o;
        return type == that.type &&
                Objects.equals(name, that.name) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(type, name, value);
    }
}
