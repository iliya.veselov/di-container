package com.fltrsh.dicontainer.reader.impl.exceptions;

import com.fltrsh.dicontainer.reader.exceptions.BeanDefinitionException;

public class NoSuchClassException extends BeanDefinitionException {
    public NoSuchClassException(String message) {
        super(message);
    }

    public NoSuchClassException(String message, Throwable cause) {
        super(message, cause);
    }
}
