package com.fltrsh.dicontainer.reader.impl.exceptions;

import com.fltrsh.dicontainer.reader.exceptions.BeanDefinitionException;

public class ReadBeanException extends BeanDefinitionException {
    public ReadBeanException(String message) {
        super(message);
    }

    public ReadBeanException(String message, Throwable cause) {
        super(message, cause);
    }
}
