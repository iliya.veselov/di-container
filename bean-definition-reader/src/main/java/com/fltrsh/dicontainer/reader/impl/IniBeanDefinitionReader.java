package com.fltrsh.dicontainer.reader.impl;


import com.fltrsh.dicontainer.reader.impl.exceptions.NoSuchResourceException;
import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.reader.BeanDefinitionReader;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IniBeanDefinitionReader implements BeanDefinitionReader {
    private RawBeanDefinitionConverter converter;

    public IniBeanDefinitionReader() {
        converter = new RawBeanDefinitionConverter();
    }

    @Override
    public Map<String, BeanDefinition> loadBeanDefinitions(String location) {
        Path resourcePath = getResourcePath(location);
        List<RawBeanDefinition> rawBeanDefinitions = getRawBeanDefinitions(resourcePath);
        return converter.convertToBeanDefinitions(rawBeanDefinitions);
    }

    private List<RawBeanDefinition> getRawBeanDefinitions(Path resourcePath) {
        List<RawBeanDefinition> rawBeanDefinitions = new ArrayList<>();
        try {
            List<String> lines = Files.lines(resourcePath).collect(Collectors.toList());
            int start = 0;
            int end;
            int size = lines.size();
            for (int i = 0; i < size; i++) {
                if (lines.get(i).isEmpty()) {
                    end = i;
                    RawBeanDefinition rawBeanDefinition = new RawBeanDefinition(lines.subList(start, end));
                    rawBeanDefinitions.add(rawBeanDefinition);
                    start = end + 1;
                }
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return rawBeanDefinitions;
    }

    private Path getResourcePath(String location) {
        try{
            return getSimpleResourcePath(location);
        }catch (FileSystemNotFoundException e){
            return getZipResourcePath(location);
        }
    }

    private Path getSimpleResourcePath(String location) {
        URL resource = this.getClass().getClassLoader().getResource(location);
        URI uri;
        if (resource == null) {
            throw new NoSuchResourceException("There is no resource " + location);
        }
        try {
            uri = resource.toURI();
        } catch (URISyntaxException e) {
            throw new NoSuchResourceException("There is no resource " + location, e);
        }
        return Paths.get(uri);
    }

    private Path getZipResourcePath(String location) {
        URL resource = this.getClass().getClassLoader().getResource(location);
        URI uri;
        if (resource == null) {
            throw new NoSuchResourceException("There is no resource " + location);
        }
        try {
            uri = resource.toURI();
        } catch (URISyntaxException e) {
            throw new NoSuchResourceException("There is no resource " + location, e);
        }
        Map<String, String> env = new HashMap<>();
        env.put("create", "true");
        try {
            FileSystem zipfs = FileSystems.newFileSystem(uri, env);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return Paths.get(uri);
    }
}