package com.fltrsh.dicontainer.reader.impl;

import com.fltrsh.dicontainer.reader.impl.exceptions.NoSuchClassException;
import com.fltrsh.dicontainer.reader.impl.exceptions.ReadBeanException;
import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.beans.BeanDefinitionConstructorArgument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class RawBeanDefinitionConverter {
    Map<String, BeanDefinition> convertToBeanDefinitions(List<RawBeanDefinition> rawBeanDefinitions) {
        Map<String, BeanDefinition> beanDefinitions = new HashMap<>();
        for (RawBeanDefinition rawBeanDefinition : rawBeanDefinitions) {
            String beanName = getBeanName(rawBeanDefinition);
            BeanDefinition beanDefinition = convertToBeanDefinition(rawBeanDefinition);
            beanDefinitions.put(beanName, beanDefinition);
        }
        return beanDefinitions;
    }

    private BeanDefinition convertToBeanDefinition(RawBeanDefinition rawBeanDefinition) {
        Class<?> beanClass = getBeanClass(rawBeanDefinition);
        List<BeanDefinitionConstructorArgument> beanConstructorArguments = getBeanConstructorArguments(rawBeanDefinition);
        List<String> beanDependencies = getBeanDependencies(beanConstructorArguments);
        return new BeanDefinition(beanClass, beanDependencies, beanConstructorArguments);
    }

    private List<BeanDefinitionConstructorArgument> getBeanConstructorArguments(RawBeanDefinition rawBeanDefinition) {
        List<BeanDefinitionConstructorArgument> constructorArguments = new ArrayList<>();
        Pattern beanConstructorArgumentRegex = Pattern.compile("([a-zA-Z_\\$][a-zA-Z\\d_\\$]*)=(ref|val):(.+)");

        for (String line : rawBeanDefinition.getRawData()) {
            Matcher matcher = beanConstructorArgumentRegex.matcher(line);
            if (matcher.find()) {
                String name = matcher.group(1);
                String value = matcher.group(3);
                BeanDefinitionConstructorArgument.Type type;

                if (matcher.group(2).equals("ref")) {
                    type = BeanDefinitionConstructorArgument.Type.BEAN;

                } else {
                    type = BeanDefinitionConstructorArgument.Type.PRIMITIVE;

                }
                constructorArguments.add(new BeanDefinitionConstructorArgument(type, name, value));
            }
        }
        return constructorArguments;
    }

    private List<String> getBeanDependencies(List<BeanDefinitionConstructorArgument> beanConstructorArguments) {
        List<String> beanDependencies = new ArrayList<>();
        for (BeanDefinitionConstructorArgument constructorArgument : beanConstructorArguments) {
            if (constructorArgument.getType() == BeanDefinitionConstructorArgument.Type.BEAN) {
                beanDependencies.add(constructorArgument.getValue());
            }
        }
        return beanDependencies;
    }

    private Class<?> getBeanClass(RawBeanDefinition rawBeanDefinition) {
        Pattern beanClassRegex = Pattern.compile("\\[([[a-zA-Z_\\$][a-zA-Z\\d_\\$]*\\.]*[a-zA-Z_$][a-zA-Z\\d_\\$]*)");
        Matcher matcher = beanClassRegex.matcher(rawBeanDefinition.getRawData().get(0));
        if (matcher.find()) {
            try {
                return Class.forName(matcher.group(1));
            } catch (ClassNotFoundException e) {
                throw new NoSuchClassException("There is no class: " + matcher.group(1), e);
            }
        } else {
            throw new ReadBeanException("Cannot read beanClass in RawBeanDefinition: " + rawBeanDefinition.toString());
        }
    }

    private String getBeanName(RawBeanDefinition rawBeanDefinition) {
        Pattern beanNameRegex = Pattern.compile(":([a-zA-Z_\\$][a-zA-Z\\d_\\$]*)]");
        Matcher matcher = beanNameRegex.matcher(rawBeanDefinition.getRawData().get(0));
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new ReadBeanException("Cannot read beanName in RawBeanDefinition: " + rawBeanDefinition.toString());
        }
    }
}
