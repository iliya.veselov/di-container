package com.fltrsh.dicontainer.reader.impl;

import java.util.List;

class RawBeanDefinition {
    private List<String> rawData;

    RawBeanDefinition(List<String> rawData) {
        this.rawData = rawData;
    }

    public List<String> getRawData() {
        return rawData;
    }

    @Override
    public String toString() {
        return rawData.toString();
    }
}
