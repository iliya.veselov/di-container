package com.fltrsh.dicontainer.reader.impl.exceptions;

import com.fltrsh.dicontainer.reader.exceptions.BeanDefinitionException;

public class NoSuchResourceException extends BeanDefinitionException {
    public NoSuchResourceException(String message) {
        super(message);
    }

    public NoSuchResourceException(String message, Throwable cause) {
        super(message, cause);
    }
}
