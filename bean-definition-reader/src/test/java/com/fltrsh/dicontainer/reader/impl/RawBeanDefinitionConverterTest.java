package com.fltrsh.dicontainer.reader.impl;

import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.beans.BeanDefinitionConstructorArgument;
import com.fltrsh.dicontainer.reader.impl.exceptions.NoSuchClassException;
import com.fltrsh.dicontainer.reader.impl.exceptions.ReadBeanException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class RawBeanDefinitionConverterTest {
    private RawBeanDefinitionConverter converter = new RawBeanDefinitionConverter();
    private List<RawBeanDefinition> rawBeanDefinitions;
    private Map<String, BeanDefinition> beanDefinitions;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUpRawBeanDefinitions(){
        rawBeanDefinitions = new ArrayList<>();
        List<String> aRawBeanData = new ArrayList<>();
        aRawBeanData.add("[com.fltrsh.dicontainer.reader.impl.test.A:a]");
        aRawBeanData.add("reference=ref:b");

        List<String> bRawBeanData = new ArrayList<>();
        bRawBeanData.add("[com.fltrsh.dicontainer.reader.impl.test.B:b]");
        bRawBeanData.add("value=val:SomeValue");

        rawBeanDefinitions.add(new RawBeanDefinition(aRawBeanData));
        rawBeanDefinitions.add(new RawBeanDefinition(bRawBeanData));
    }

    @Before
    public void setUpBeanDefinitions() throws ClassNotFoundException {
        beanDefinitions = new HashMap<>();

        BeanDefinitionConstructorArgument aConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.BEAN,
                "reference",
                "b"
        );
        BeanDefinition aBeanDefinition = new BeanDefinition(
                Class.forName("com.fltrsh.dicontainer.reader.impl.test.A"),
                Arrays.asList("b"),
                Arrays.asList(aConstructorArgument)
                );

        BeanDefinitionConstructorArgument bConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.PRIMITIVE,
                "value",
                "SomeValue"
        );
        BeanDefinition bBeanDefinition = new BeanDefinition(
                Class.forName("com.fltrsh.dicontainer.reader.impl.test.B"),
                Collections.emptyList(),
                Arrays.asList(bConstructorArgument)
        );

        beanDefinitions.put("a", aBeanDefinition);
        beanDefinitions.put("b", bBeanDefinition);
    }

    @Test
    public void convertingCorrectRawBeans(){
        //WHEN
        Map<String, BeanDefinition> actualBeanDefinitions = converter.convertToBeanDefinitions(rawBeanDefinitions);
        //THEN
        assertEquals(beanDefinitions, actualBeanDefinitions);
    }

    @Test
    public void convertingRawBeanWithUnexistingClass(){
        //GIVEN
        List<String> aRawBeanData = new ArrayList<>();
        aRawBeanData.add("[com.fltrsh.dicontainer.reader.impl.test.THERE_IS_NO_SUCH_CLASS:a]");
        aRawBeanData.add("reference=ref:b");
        rawBeanDefinitions = new ArrayList<>();
        rawBeanDefinitions.add(new RawBeanDefinition(aRawBeanData));

        expectedException.expect(NoSuchClassException.class);
        expectedException.expectMessage("There is no class: com.fltrsh.dicontainer.reader.impl.test.THERE_IS_NO_SUCH_CLASS");
        //WHEN
        converter.convertToBeanDefinitions(rawBeanDefinitions);
    }

    @Test
    public void convertingRawBeanWithIncorrectClassDefinitionInContext(){
        //GIVEN
        List<String> aRawBeanData = new ArrayList<>();
        aRawBeanData.add("[{}{]]]$$2124com.fltrsh.dicontainer.reader.impl.test.A:a]");
        aRawBeanData.add("reference=ref:b");
        rawBeanDefinitions = new ArrayList<>();
        rawBeanDefinitions.add(new RawBeanDefinition(aRawBeanData));

        expectedException.expect(ReadBeanException.class);
        expectedException.expectMessage("Cannot read beanClass in RawBeanDefinition: [[{}{]]]$$2124com.fltrsh.dicontainer.reader.impl.test.A:a], reference=ref:b]");
        //WHEN
        converter.convertToBeanDefinitions(rawBeanDefinitions);
    }@Test
    public void convertingRawBeanWithIncorrectNameInContext(){
        //GIVEN
        List<String> aRawBeanData = new ArrayList<>();
        aRawBeanData.add("[com.fltrsh.dicontainer.reader.impl.test.A:a|/**]");
        aRawBeanData.add("reference=ref:b");
        rawBeanDefinitions = new ArrayList<>();
        rawBeanDefinitions.add(new RawBeanDefinition(aRawBeanData));

        expectedException.expect(ReadBeanException.class);
        expectedException.expectMessage("Cannot read beanName in RawBeanDefinition: [[com.fltrsh.dicontainer.reader.impl.test.A:a|/**], reference=ref:b]");
        //WHEN
        converter.convertToBeanDefinitions(rawBeanDefinitions);
    }
}