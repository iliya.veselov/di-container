package com.fltrsh.dicontainer.reader.impl.test;

import java.util.Objects;

public class B {
    private String value;

    public B(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        B b = (B) o;
        return Objects.equals(value, b.value);
    }
}
