package com.fltrsh.dicontainer.reader.impl.test;

import java.util.Objects;

public class A {
    private B reference;

    public A(B reference) {
        this.reference = reference;
    }

    public B getReference() {
        return reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        A a = (A) o;
        return Objects.equals(reference, a.reference);
    }
}
