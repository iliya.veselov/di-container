package com.fltrsh.dicontainer.reader.impl;


import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.beans.BeanDefinitionConstructorArgument;
import com.fltrsh.dicontainer.reader.impl.exceptions.NoSuchResourceException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class IniBeanDefinitionReaderTest {

    private Map<String, BeanDefinition> beanDefinitions;
    private IniBeanDefinitionReader beanDefinitionReader = new IniBeanDefinitionReader();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUpBeanDefinitions() throws ClassNotFoundException {
        beanDefinitions = new HashMap<>();

        BeanDefinitionConstructorArgument aConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.BEAN,
                "reference",
                "b"
        );
        BeanDefinition aBeanDefinition = new BeanDefinition(
                Class.forName("com.fltrsh.dicontainer.reader.impl.test.A"),
                Arrays.asList("b"),
                Arrays.asList(aConstructorArgument)
        );

        BeanDefinitionConstructorArgument bConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.PRIMITIVE,
                "value",
                "SomeValue"
        );
        BeanDefinition bBeanDefinition = new BeanDefinition(
                Class.forName("com.fltrsh.dicontainer.reader.impl.test.B"),
                Collections.emptyList(),
                Arrays.asList(bConstructorArgument)
        );

        beanDefinitions.put("a", aBeanDefinition);
        beanDefinitions.put("b", bBeanDefinition);
    }

    @Test
    public void readingCorrectContextAndLoadingBeanDefinitions(){
        //WHEN
        Map<String, BeanDefinition> actualBeanDefinitions = beanDefinitionReader.loadBeanDefinitions("com/fltrsh/dicontainer/reader/impl/context.ini");
        //THEN
        assertEquals(beanDefinitions, actualBeanDefinitions);
    }

    @Test
    public void readingUnexistingContext(){
        //GIVEN
        expectedException.expect(NoSuchResourceException.class);
        expectedException.expectMessage("There is no resource com/fltrsh/dicontainer/reader/impl/NORESOURCE.ini");
        //WHEN
        beanDefinitionReader.loadBeanDefinitions("com/fltrsh/dicontainer/reader/impl/NORESOURCE.ini");
    }

}
