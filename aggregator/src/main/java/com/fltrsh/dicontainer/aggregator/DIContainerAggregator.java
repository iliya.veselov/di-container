package com.fltrsh.dicontainer.aggregator;

import com.fltrsh.dicontainer.container.DIContainer;
import com.fltrsh.dicontainer.container.impl.SimpleDIContainer;
import com.fltrsh.dicontainer.reader.BeanDefinitionReader;
import com.fltrsh.dicontainer.reader.impl.IniBeanDefinitionReader;

public class DIContainerAggregator {
    private static DIContainer container;

    public DIContainerAggregator(String resourceLocation){
        BeanDefinitionReader beanDefinitionReader = new IniBeanDefinitionReader();
        container = new SimpleDIContainer(resourceLocation, beanDefinitionReader);
    }

    public DIContainer getDIContainer(){
        return container;
    }
}
