package com.fltrsh.dicontainer.movieexample;

import com.fltrsh.dicontainer.aggregator.DIContainerAggregator;
import com.fltrsh.dicontainer.container.DIContainer;

public class MovieExample {
    public static void main(String[] args) {
        DIContainerAggregator aggregator = new DIContainerAggregator("com/fltrsh/dicontainer/movieexample/context.ini");
        DIContainer container = aggregator.getDIContainer();
        MovieLister movieLister = (MovieLister) container.getBean("movieLister");
        movieLister.listMovies();
    }
}
