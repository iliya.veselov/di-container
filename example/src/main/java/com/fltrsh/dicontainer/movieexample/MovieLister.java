package com.fltrsh.dicontainer.movieexample;

public class MovieLister {
        private MovieFinder movieFinder;

    public MovieLister(MovieFinder movieFinder) {
        this.movieFinder = movieFinder;
    }

    public void listMovies(){
            System.out.println("There are movies: " + movieFinder.getAll());
        }
}
