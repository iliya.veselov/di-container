package com.fltrsh.dicontainer.movieexample;

import java.util.Arrays;
import java.util.List;

public class MovieFinder{
    private List<String> movies;

    public MovieFinder(String movies) {
        this.movies = Arrays.asList(movies);
    }

    public List<String> getAll() {
        return movies;
    }
}
