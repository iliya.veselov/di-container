package com.fltrsh.dicontainer.graphexample;

public class H3 {
    private G2 g2;

    public H3(G2 g2) {
        this.g2 = g2;
    }

    @Override
    public String toString() {
        return "H3 has dependencies { " + g2 + "}\n";
    }
}
