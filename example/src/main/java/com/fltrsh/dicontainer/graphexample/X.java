package com.fltrsh.dicontainer.graphexample;

public class X {
    private H1 h1;
    private H2 h2;
    private H3 h3;

    public X(H1 h1, H2 h2, H3 h3) {
        this.h1 = h1;
        this.h2 = h2;
        this.h3 = h3;
    }

    @Override
    public String toString(){
        return "X has dependencies { " + h1 + h2 + h3 + " }\n";
    }
}
