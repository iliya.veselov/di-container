package com.fltrsh.dicontainer.graphexample;

public class G2 {
    private F f;

    public G2(F f) {
        this.f = f;
    }

    @Override
    public String toString() {
        return "G2 has dependencies { " + f + " }\n";
    }
}
