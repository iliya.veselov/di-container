package com.fltrsh.dicontainer.graphexample;

public class H1 {
    private G1 g1;

    public H1(G1 g1) {
        this.g1 = g1;
    }

    @Override
    public String toString() {
        return "H1 has dependencies {" + g1 + " }\n";
    }
}
