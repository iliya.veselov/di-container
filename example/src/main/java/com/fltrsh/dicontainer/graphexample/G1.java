package com.fltrsh.dicontainer.graphexample;

public class G1 {
    private F f;

    public G1(F f) {
        this.f = f;
    }

    @Override
    public String toString() {
        return "G1 has dependencies { " + f + " }\n";
    }
}
