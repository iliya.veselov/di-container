package com.fltrsh.dicontainer.graphexample;

import com.fltrsh.dicontainer.aggregator.DIContainerAggregator;
import com.fltrsh.dicontainer.container.DIContainer;

public class GraphExample {
    public static void main(String[] args) {
        DIContainerAggregator aggregator = new DIContainerAggregator("com/fltrsh/dicontainer/graphexample/context.ini");
        DIContainer container = aggregator.getDIContainer();
        X x = (X) container.getBean("x");
        System.out.println(x);
    }
}
