package com.fltrsh.dicontainer.graphexample;

public class H2 {
    private G1 g1;
    private G2 g2;

    public H2(G1 g1, G2 g2) {
        this.g1 = g1;
        this.g2 = g2;
    }

    @Override
    public String toString() {
        return "H2 has dependencies { " + g1 + g2 + " }\n";
    }
}
