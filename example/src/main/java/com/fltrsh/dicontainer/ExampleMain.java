package com.fltrsh.dicontainer;

import com.fltrsh.dicontainer.graphexample.GraphExample;
import com.fltrsh.dicontainer.movieexample.MovieExample;

public class ExampleMain {
    public static void main(String[] args) {
        System.out.println("MOVIE EXAMPLE");
        MovieExample.main(args);
        System.out.println("");

        System.out.println("GRAPH EXAMPLE");
        GraphExample.main(args);
    }
}
