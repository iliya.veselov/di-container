package com.fltrsh.dicontainer.container.impl.exceptions;

import com.fltrsh.dicontainer.container.exceptions.ContainerException;

public class BeanInstantiationException extends ContainerException {
    public BeanInstantiationException(String message) {
        super(message);
    }

    public BeanInstantiationException(String message, Throwable cause) {
        super(message, cause);
    }
}
