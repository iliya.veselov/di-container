package com.fltrsh.dicontainer.container.impl.exceptions;

import com.fltrsh.dicontainer.container.exceptions.ContainerException;

public class NoSuchBeanException extends ContainerException {

    public NoSuchBeanException(String message) {
        super(message);
    }

    public NoSuchBeanException(String message, Throwable cause) {
        super(message, cause);
    }
}
