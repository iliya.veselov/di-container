package com.fltrsh.dicontainer.container.impl.exceptions;

import com.fltrsh.dicontainer.container.exceptions.ContainerException;

public class CircularDependencyException extends ContainerException {
    public CircularDependencyException(String message) {
        super(message);
    }

    public CircularDependencyException(String message, Throwable cause) {
        super(message, cause);
    }
}
