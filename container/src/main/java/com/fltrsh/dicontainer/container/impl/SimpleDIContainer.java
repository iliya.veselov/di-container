package com.fltrsh.dicontainer.container.impl;

import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.container.DIContainer;
import com.fltrsh.dicontainer.container.impl.exceptions.BeanInstantiationException;
import com.fltrsh.dicontainer.container.impl.exceptions.CircularDependencyException;
import com.fltrsh.dicontainer.container.impl.exceptions.NoSuchBeanException;
import com.fltrsh.dicontainer.reader.BeanDefinitionReader;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleDIContainer implements DIContainer {

    private Map<String, FactoryBean> container;

    public SimpleDIContainer(String resourceLocation, BeanDefinitionReader beanDefinitionReader) {
        Map<String, BeanDefinition> beanDefinitions = beanDefinitionReader.loadBeanDefinitions(resourceLocation);
        BeanDefinitionConverter beanDefinitionConverter = new BeanDefinitionConverter();
        container = beanDefinitionConverter.convertToFactoryBeans(beanDefinitions);
        checkIfContainerHasCycles();
    }

    @Override
    public Object getBean(String beanName) {
        FactoryBean factoryBean = container.get(beanName);
        if (factoryBean == null) {
            throw new NoSuchBeanException("There is no such bean: beanName = " + beanName);
        }
        if (factoryBean.getInstance() == null) {
            Object instance = createBean(factoryBean);
            factoryBean.setInstance(instance);
            return instance;
        }
        return factoryBean.getInstance();
    }

    private Object createBean(FactoryBean factoryBean) {
        Map<String, Object> createdBeanDependencies = getBeanDependencies(factoryBean.getDependencies());
        injectCreatedDependenciesIntoFactoryBean(createdBeanDependencies, factoryBean);

        Constructor<?> constructor = factoryBean.getBeanClass().getDeclaredConstructors()[0];
        Object[] constructorArguments = getConstructorArguments(factoryBean);
        try {
            return constructor.newInstance(constructorArguments);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new BeanInstantiationException("Bean can't be instantiated.", e);
        }
    }

    private Object[] getConstructorArguments(FactoryBean factoryBean) {
        List<FactoryBeanConstructorArgument> factoryBeanConstructorArguments = factoryBean.getConstructorArguments();
        int argumentNumber = factoryBeanConstructorArguments.size();
        Object[] constructorArguments = new Object[argumentNumber];
        for (int i = 0; i < argumentNumber; i++) {
            constructorArguments[i] = factoryBeanConstructorArguments.get(i).getValue();
        }
        return constructorArguments;
    }

    private void injectCreatedDependenciesIntoFactoryBean(Map<String, Object> createdBeanDependencies, FactoryBean factoryBean) {
        for (FactoryBeanConstructorArgument constructorArgument : factoryBean.getConstructorArguments()) {
            if (!constructorArgument.isBean()) {
                continue;
            }

            Object dependency = createdBeanDependencies.get(constructorArgument.getValue());
            if (dependency == null) {
                throw new NoSuchBeanException("There is no such bean dependency: beanName = " + constructorArgument.getValue());
            }
            constructorArgument.setBeanValue(dependency);
        }
    }

    private Map<String, Object> getBeanDependencies(List<String> dependencies) {

        Map<String, Object> beanDependencies = new HashMap<>();
        for (String dependencyName : dependencies) {
            beanDependencies.put(dependencyName, getBean(dependencyName));
        }
        return beanDependencies;
    }

    private void checkIfContainerHasCycles() {
        container.forEach((this::depthFirstSearch));
    }

    private void depthFirstSearch(String beanName, FactoryBean factoryBean) {
        if (factoryBean.getStatus() == FactoryBean.Status.VISITED) {
            throw new CircularDependencyException("There is circular dependency: beanName = " + beanName);
        }
        if (factoryBean.getStatus() == FactoryBean.Status.CHECKED) {
            return;
        }

        factoryBean.setStatus(FactoryBean.Status.VISITED);
        for (String dependencyName : factoryBean.getDependencies()) {
            FactoryBean dependency = container.get(dependencyName);
            if (dependency == null) {
                throw new NoSuchBeanException("There is no such bean: beanName = " + dependencyName);
            }
            depthFirstSearch(dependencyName, dependency);
        }
        factoryBean.setStatus(FactoryBean.Status.CHECKED);
    }



}



