package com.fltrsh.dicontainer.container.impl;

import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.beans.BeanDefinitionConstructorArgument;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.*;

class BeanDefinitionConverter {

    Map<String, FactoryBean> convertToFactoryBeans(Map<String, BeanDefinition> beanDefinitions) {
        Map<String, FactoryBean> factoryBeans = new HashMap<>();
        beanDefinitions.forEach((beanName, beanDefinition) -> {
            factoryBeans.put(beanName, convertToFactoryBean(beanDefinition));
        });
        return factoryBeans;
    }

    private FactoryBean convertToFactoryBean(BeanDefinition beanDefinition) {
        Class<?> beanClass = beanDefinition.getBeanClass();
        List<String> dependencies = beanDefinition.getDependencies();
        List<FactoryBeanConstructorArgument> constructorArguments = extractConstructorArguments(beanDefinition);
        return new FactoryBean(
                beanClass,
                dependencies,
                constructorArguments);
    }

    private List<FactoryBeanConstructorArgument> extractConstructorArguments(BeanDefinition beanDefinition) {
        List<FactoryBeanConstructorArgument> factoryBeanConstructorArguments = new ArrayList<>();
        List<BeanDefinitionConstructorArgument> constructorArguments = beanDefinition.getConstructorArguments();

        Constructor<?> constructor = beanDefinition.getBeanClass().getDeclaredConstructors()[0];
        Parameter[] parameters = constructor.getParameters();

        for (BeanDefinitionConstructorArgument constructorArgument : constructorArguments) {
            for (int i = 0; i < parameters.length; i++) {
                if (constructorArgument.getName().equals(parameters[i].getName())) {
                    Class<?> argumentClass = parameters[i].getType();
                    Object value = castValue(constructorArgument, argumentClass);

                    FactoryBeanConstructorArgument factoryBeanConstructorArgument = new FactoryBeanConstructorArgument(
                            argumentClass,
                            constructorArgument.getName(),
                            value,
                            isBean(constructorArgument),
                            i
                    );
                    factoryBeanConstructorArguments.add(factoryBeanConstructorArgument);
                }
            }
        }
        sortConstructorArguments(factoryBeanConstructorArguments);
        return factoryBeanConstructorArguments;
    }

    private void sortConstructorArguments(List<FactoryBeanConstructorArgument> factoryBeanConstructorArguments) {
        factoryBeanConstructorArguments.sort((arg1, arg2) -> {
            int result = arg1.getArgumentIndex().compareTo(arg2.getArgumentIndex());
            if (result == 0) {
                throw new IllegalStateException("Constructor arguments can not have same index");
            }
            return result;
        });
    }

    private boolean isBean(BeanDefinitionConstructorArgument constructorArgument) {
        return constructorArgument.getType() == BeanDefinitionConstructorArgument.Type.BEAN;
    }

    private Object castValue(BeanDefinitionConstructorArgument constructorArgument, Class<?> argumentClass) {
        if (argumentClass.isPrimitive()) {
            return parsePrimitiveArgumentValue(constructorArgument, argumentClass);

        }
        return constructorArgument.getValue();
    }

    private Object parsePrimitiveArgumentValue(BeanDefinitionConstructorArgument constructorArgument, Class<?> argumentClass) {
        Object value = null;
        switch (argumentClass.getName()) {
            case "byte":
                value = Byte.parseByte(constructorArgument.getValue());
                break;
            case "short":
                value = Short.parseShort(constructorArgument.getValue());
                break;
            case "int":
                value = Integer.parseInt(constructorArgument.getValue());
                break;
            case "long":
                value = Long.parseLong(constructorArgument.getValue());
                break;
            case "float":
                value = Float.parseFloat(constructorArgument.getValue());
                break;
            case "double":
                value = Double.parseDouble(constructorArgument.getValue());
                break;

        }
        return value;
    }

}
