package com.fltrsh.dicontainer.container.impl;

import java.util.List;
import java.util.Objects;

class FactoryBean {
    private Class<?> beanClass;
    private List<String> dependencies;

    private List<FactoryBeanConstructorArgument> constructorArguments;

    private Object instance;
    private Status status = Status.UNVISITED;
    public FactoryBean(Class<?> beanClass, List<String> dependencies, List<FactoryBeanConstructorArgument> constructorArguments) {
        this.beanClass = beanClass;
        this.dependencies = dependencies;
        this.constructorArguments = constructorArguments;
    }

    public Class<?> getBeanClass() {
        return beanClass;
    }

    public List<String> getDependencies() {
        return dependencies;
    }

    public List<FactoryBeanConstructorArgument> getConstructorArguments() {
        return constructorArguments;
    }

    public Object getInstance() {
        return instance;
    }

    public void setInstance(Object instance) {
        this.instance = instance;
    }

    public void setStatus(Status status) {

        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public enum Status {
        UNVISITED,
        VISITED,
        CHECKED;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FactoryBean that = (FactoryBean) o;
        return Objects.equals(beanClass, that.beanClass) &&
                Objects.equals(dependencies, that.dependencies) &&
                Objects.equals(constructorArguments, that.constructorArguments) &&
                Objects.equals(instance, that.instance) &&
                status == that.status;
    }

    @Override
    public int hashCode() {

        return Objects.hash(beanClass, dependencies, constructorArguments, instance, status);
    }

    @Override
    public String toString() {
        return "FactoryBean{" +
                "beanClass=" + beanClass +
                ", dependencies=" + dependencies +
                ", constructorArguments=" + constructorArguments +
                ", instance=" + instance +
                ", status=" + status +
                '}';
    }
}
