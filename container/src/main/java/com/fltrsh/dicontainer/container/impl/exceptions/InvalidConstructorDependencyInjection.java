package com.fltrsh.dicontainer.container.impl.exceptions;

import com.fltrsh.dicontainer.container.exceptions.ContainerException;

public class InvalidConstructorDependencyInjection extends ContainerException {

    public InvalidConstructorDependencyInjection(String message) {
        super(message);
    }

    public InvalidConstructorDependencyInjection(String message, Throwable cause) {
        super(message, cause);
    }
}
