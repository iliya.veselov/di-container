package com.fltrsh.dicontainer.container.impl;

import com.fltrsh.dicontainer.container.impl.exceptions.InvalidConstructorDependencyInjection;

import java.util.Objects;

class FactoryBeanConstructorArgument {
    private Class<?> type;
    private String name;
    private Object value;
    private Boolean isBean;
    private Integer argumentIndex;

    public FactoryBeanConstructorArgument(Class<?> type, String name, Object value, Boolean isBean, Integer argumentIndex) {
        this.type = type;
        this.name = name;
        this.value = value;
        this.isBean = isBean;
        this.argumentIndex = argumentIndex;
    }

    public Class<?> getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setBeanValue(Object value) {
        if (value.getClass().equals(type)) {
            this.value = value;
        }else {
            throw new InvalidConstructorDependencyInjection("Argument constructor class and injected dependency class are different: constructorClass = " + type + " injectedDependencyClass = " + value.getClass());
        }
    }

    public Boolean isBean() {
        return isBean;
    }

    public Integer getArgumentIndex() {
        return argumentIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FactoryBeanConstructorArgument that = (FactoryBeanConstructorArgument) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(name, that.name) &&
                Objects.equals(value, that.value) &&
                Objects.equals(isBean, that.isBean) &&
                Objects.equals(argumentIndex, that.argumentIndex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, value, isBean, argumentIndex);
    }

    @Override
    public String toString() {
        return "FactoryBeanConstructorArgument{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", isBean=" + isBean +
                ", argumentIndex=" + argumentIndex +
                '}';
    }
}
