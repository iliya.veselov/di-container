package com.fltrsh.dicontainer.container.impl;

import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.beans.BeanDefinitionConstructorArgument;
import com.fltrsh.dicontainer.container.impl.exceptions.CircularDependencyException;
import com.fltrsh.dicontainer.container.impl.exceptions.NoSuchBeanException;
import com.fltrsh.dicontainer.container.impl.test.circular.X;
import com.fltrsh.dicontainer.container.impl.test.circular.Y;
import com.fltrsh.dicontainer.container.impl.test.circular.Z;
import com.fltrsh.dicontainer.container.impl.test.correct.A;
import com.fltrsh.dicontainer.container.impl.test.correct.B;
import com.fltrsh.dicontainer.reader.BeanDefinitionReader;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SimpleDIContainerTest {

    @Mock
    private BeanDefinitionReader beanDefinitionReader;
    private SimpleDIContainer diContainer;

    private Map<String, BeanDefinition> beanDefinitions;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void gettingCorrectBeans() {
        //GIVEN
        B expectedB = new B("SomeValue");
        A expectedA = new A(expectedB);
        setUpCorrectBeanDefinitions();
        when(beanDefinitionReader.loadBeanDefinitions("someContext.ini")).thenReturn(beanDefinitions);
        //WHEN
        diContainer = new SimpleDIContainer("someContext.ini", beanDefinitionReader);
        A actualA = (A) diContainer.getBean("a");
        B actualB = (B) diContainer.getBean("b");
        //THEN
        verify(beanDefinitionReader, times(1)).loadBeanDefinitions("someContext.ini");
        assertEquals(expectedA, actualA);
        assertEquals(expectedB, actualB);
    }

    @Test
    public void gettingUnexistingBean() {
        //GIVEN
        setUpCorrectBeanDefinitions();
        when(beanDefinitionReader.loadBeanDefinitions("someContext.ini")).thenReturn(beanDefinitions);
        expectedException.expect(NoSuchBeanException.class);
        expectedException.expectMessage("There is no such bean: beanName = abracadabra");
        //WHEN
        diContainer = new SimpleDIContainer("someContext.ini", beanDefinitionReader);
        //THEN
        verify(beanDefinitionReader, times(1)).loadBeanDefinitions("someContext.ini");
        //WHEN
        diContainer.getBean("abracadabra");
    }

    @Test
    public void creatingCircularDependency() {
        //GIVEN
        setUpCircularDependencyBeanDefinitions();
        when(beanDefinitionReader.loadBeanDefinitions("someContext.ini")).thenReturn(beanDefinitions);
        expectedException.expect(CircularDependencyException.class);
        expectedException.expectMessage("There is circular dependency: beanName = x");
        //WHEN
        diContainer = new SimpleDIContainer("someContext.ini", beanDefinitionReader);
    }

    private void setUpCorrectBeanDefinitions() {
        beanDefinitions = new HashMap<>();

        BeanDefinitionConstructorArgument aConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.BEAN,
                "reference",
                "b"
        );

        BeanDefinitionConstructorArgument bConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.PRIMITIVE,
                "value",
                "SomeValue"
        );

        BeanDefinition aBeanDefinition = new BeanDefinition(
                A.class,
                Arrays.asList("b"),
                Arrays.asList(aConstructorArgument)
        );
        BeanDefinition bBeanDefinition = new BeanDefinition(
                B.class,
                Collections.emptyList(),
                Arrays.asList(bConstructorArgument)
        );
        beanDefinitions.put("a", aBeanDefinition);
        beanDefinitions.put("b", bBeanDefinition);

    }

    private void setUpCircularDependencyBeanDefinitions() {
        beanDefinitions = new HashMap<>();

        BeanDefinitionConstructorArgument xConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.BEAN,
                "reference",
                "y"
        );

        BeanDefinitionConstructorArgument yConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.BEAN,
                "reference",
                "z"
        );

        BeanDefinitionConstructorArgument zConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.BEAN,
                "reference",
                "x"
        );
        BeanDefinition xBeanDefinition = new BeanDefinition(
                X.class,
                Arrays.asList("y"),
                Arrays.asList(xConstructorArgument)
        );

        BeanDefinition yBeanDefinition = new BeanDefinition(
                Y.class,
                Arrays.asList("z"),
                Arrays.asList(yConstructorArgument)
        );

        BeanDefinition zBeanDefinition = new BeanDefinition(
                Z.class,
                Arrays.asList("x"),
                Arrays.asList(zConstructorArgument)
        );

        beanDefinitions.put("x", xBeanDefinition);
        beanDefinitions.put("y", yBeanDefinition);
        beanDefinitions.put("z", zBeanDefinition);
    }

}