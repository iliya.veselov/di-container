package com.fltrsh.dicontainer.container.impl;

import com.fltrsh.dicontainer.beans.BeanDefinition;
import com.fltrsh.dicontainer.beans.BeanDefinitionConstructorArgument;
import com.fltrsh.dicontainer.container.impl.test.correct.A;
import com.fltrsh.dicontainer.container.impl.test.correct.B;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class BeanDefinitionConverterTest {

    private Map<String, BeanDefinition> beanDefinitions;
    private Map<String, FactoryBean> factoryBeans;
    private BeanDefinitionConverter converter = new BeanDefinitionConverter();

    @Test
    public void convertingCorrectBeanDefinitions(){
        //GIVEN
        setUpCorrectBeanDefinitions();
        setUpFactoryBeans();
        //WHEN
        Map<String, FactoryBean> actualFactoryBeans = converter.convertToFactoryBeans(beanDefinitions);
        //THEN
        assertEquals(factoryBeans,actualFactoryBeans);
    }

    private void setUpCorrectBeanDefinitions() {
        beanDefinitions = new HashMap<>();

        BeanDefinitionConstructorArgument aConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.BEAN,
                "reference",
                "b"
        );

        BeanDefinitionConstructorArgument bConstructorArgument = new BeanDefinitionConstructorArgument(
                BeanDefinitionConstructorArgument.Type.PRIMITIVE,
                "value",
                "SomeValue"
        );

        BeanDefinition aBeanDefinition = new BeanDefinition(
                A.class,
                Arrays.asList("b"),
                Arrays.asList(aConstructorArgument)
        );
        beanDefinitions.put("a", aBeanDefinition);

        BeanDefinition bBeanDefinition = new BeanDefinition(
                B.class,
                Collections.emptyList(),
                Arrays.asList(bConstructorArgument)
        );
        beanDefinitions.put("b", bBeanDefinition);


    }

    private void setUpFactoryBeans() {
        factoryBeans = new HashMap<>();

        FactoryBeanConstructorArgument aConstructorArgument = new FactoryBeanConstructorArgument(
                B.class,
                "reference",
                "b",
                true,
                0
        );

        FactoryBeanConstructorArgument bConstructorArgument = new FactoryBeanConstructorArgument(
                String.class,
                "value",
                "SomeValue",
                false,
                0
        );

        FactoryBean aFactoryBean = new FactoryBean(
                A.class,
                Arrays.asList("b"),
                Arrays.asList(aConstructorArgument)
                );
        FactoryBean bFactoryBean = new FactoryBean(
                B.class,
                Arrays.asList(new String[]{}),
                Arrays.asList(bConstructorArgument)
        );

        factoryBeans.put("a", aFactoryBean);
        factoryBeans.put("b", bFactoryBean);
    }
}