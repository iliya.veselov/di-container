package com.fltrsh.dicontainer.container;

public interface DIContainer {
    Object getBean(String beanName);
}
