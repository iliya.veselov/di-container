package com.fltrsh.dicontainer.reader;

import com.fltrsh.dicontainer.beans.BeanDefinition;

import java.util.Map;

public interface BeanDefinitionReader {
    Map<String, BeanDefinition> loadBeanDefinitions(String location);
}
